'use strict';
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= template/vendor/modernizr-3.7.1.min.js
//= template/owl.carousel.min.js
//= template/jquery.magnific-popup.min.js
//= template/jquery.counterup.min.js
//= template/jquery.countdown.js
//= template/jquery.ui.js
//= template/isotope.pkgd.min.js
//= template/slinky.menu.js
//= template/jquery.instagramFeed.min.js
//= template/plugins.js
//= template/main.js



$(document).ready(function () {
	/*----------------------------
	Cart Plus Minus Button
	------------------------------ */
	var CartPlusMinus = jQuery('.cart-plus-minus');
	CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
	CartPlusMinus.append('<div class="inc qtybutton">+</div>');
	jQuery(".qtybutton").on("click", function() {
		var $button = jQuery(this);
		var oldValue = $button.parent().find("input").val();
		if ($button.text() === "+") {
			var newVal = parseFloat(oldValue) + 1;
		} else {
		// Don't allow decrementing below zero
			console.log(oldValue);
			if (oldValue > 0) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 0;
			}
		}
		$button.parent().find("input").val(newVal);
		inputChangeCount();
	});
	$(".cart-plus-minus .cart-plus-minus-box").change(function () {
		inputChangeCount();
	});
	function inputChangeCount() {
		console.log("chenge number");
	}


	// показать каталог в меню
    $(".btn-catalog-drop").on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();

        $(this).find(".catalog-drop").addClass("show");
        $("body").css("overflow", "hidden");
    });

    $(".btn-catalog-drop .catalog-drop").on("click", function (e) {
        e.stopPropagation();
    });
    $(".catalog-drop .catalog-drop-close").on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();

        $(".catalog-drop").removeClass("show");
        $("body").css("overflow", "auto");
    });
});
